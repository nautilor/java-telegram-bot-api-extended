package com.nautilor.telegrambot.extended.listener;

import com.nautilor.telegrambot.extended.handler.UpdateHandler;
import com.pengrad.telegrambot.ExceptionHandler;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UpdateListener implements UpdatesListener {

    private final String HASH_PATTERN = "#\\w+";
    private String token;

    private TelegramBot bot;
    private ExceptionHandler errorHandler;

    private final Map<String, UpdateHandler> commandHandler = new HashMap<>();
    private final Map<String, UpdateHandler> hashHandler = new HashMap<>();
    private final Map<String, UpdateHandler> messageHandler = new HashMap<>();
    private final Map<String, UpdateHandler> callbackHandler = new HashMap<>();

    private UpdateHandler inlineQueryHandler;
    private UpdateHandler photoHandler;
    private UpdateHandler defaultHandler;

    @Override
    public int process(List<Update> list) {
        list.forEach(this::process);
        return CONFIRMED_UPDATES_ALL;
    }

    /**
     * Start the telegram bot by using a previously set token and {@link ExceptionHandler}
     */
    public void listen() {
        if (token == null || token.equals("")) {
            throw new RuntimeException("Telegram token cannot be null");
        }
        bot = new TelegramBot(token);
        if (errorHandler == null) {
            bot.setUpdatesListener(this);
            return;
        }
        bot.setUpdatesListener(this, errorHandler);
    }

    /**
     * Set the Telegram token to use
     * @param token The Telegram bot token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Set an {@link ExceptionHandler} to use
     * @param handler The {@link ExceptionHandler} implementation
     */
    public void setErrorHandler(ExceptionHandler handler) {
        this.errorHandler = handler;
    }

    /**
     * Set an Handler for a specific command
     * @param command The {@link String} representing the command you want to handle
     * @param handler The class of type {@link UpdateHandler} that is going to handle the {@link Update}
     * @return void
     */
    public void addCommandHandler(String command, UpdateHandler handler) {
        commandHandler.putIfAbsent(command, handler);
    }

    /**
     * Set an Handler for photo updates
     * @param handler The class of type {@link UpdateHandler} that is going to handle the {@link Update}
     */
    public void addPhotoHandler(UpdateHandler handler) {
        photoHandler = handler;
    }

    /**
     * Set an Handler for inlineQuery
     * @param handler The class of type {@link UpdateHandler} that is going to handle the {@link Update}
     */
    public void addInlineQueryHandler(UpdateHandler handler) {
        inlineQueryHandler = handler;
    }

    /**
     * Set an Handler for a Messages based on a regex
     * @param regex     the regex used to match the message
     * @param handler   the {@link UpdateHandler} to handle the update
     */
    public void addMessageHandler(String regex, UpdateHandler handler) {
        messageHandler.putIfAbsent(regex, handler);
    }

    /**
     * Set an Handler for a specific CallbackQuery
     * @param regex The {@link String} representing the callback you want to handle
     * @param handler The class of type {@link UpdateHandler} that is going to handle the {@link Update}
     */
    public void addCallbackHandler(String regex, UpdateHandler handler) {
        callbackHandler.putIfAbsent(regex, handler);
    }

    /**
     * Set an Handler for a specific hashtag
     * @param hash The {@link String} representing the hashtag you want to handle
     * @param handler The class of type {@link UpdateHandler} that is going to handle the {@link Update}
     */
    public void addHashHandler(String hash, UpdateHandler handler) {
        hashHandler.putIfAbsent(hash, handler);
    }

    /**
     * Set an Handler for generic updates
     * @param handler The class of type {@link UpdateHandler} that is going to handle the {@link Update}
     */
    public void addDefaultHandler(UpdateHandler handler) {
        defaultHandler = handler;
    }

    /**
     * extract the message of any type from the update
     * @param update The {@link Update} received from telegram
     * @return the {@link Message} extracted
     */
    private Message retrieveMessage(Update update) {
        Message message = (update.editedMessage() == null) ? update.message() : update.editedMessage();
        if (message == null) {
            return (update.editedChannelPost() == null) ? update.channelPost() : update.editedChannelPost();
        }
        return message;
    }

    /**
     * Extract the text from the message
     * @param message The telegram {@link Message} from the update
     * @return The String representing the text of the message
     */
    private String retrieveText(Message message) {
        return (message.text() != null) ? message.text() : message.caption();
    }

    /**
     * Check if there's a command in the current update and if it's inside {@link #commandHandler}
     * @param update The {@link Update} received from telegram
     * @return if the update contains a command
     */
    private boolean isCommand(Update update) {
        Message message = retrieveMessage(update);
        if (message == null) {
            return false;
        }
        String messageText = retrieveText(message);
        if (messageText == null) {
            return false;
        }
        if (!messageText.startsWith("/")) {
            return false;
        }
        String command = messageText.contains(" ") ? messageText.split(" ")[0] : messageText;
        command = command.replace("/", "");
        return commandHandler.containsKey(command);
    }

    /**
     * Search over {@link #commandHandler} for a {@link UpdateHandler} for the specific command
     * @param update The {@link Update} received from telegram
     * @return void
     */
    private void handleCommand(Update update) {
        Message message = retrieveMessage(update);
        String messageText = retrieveText(message);
        messageText = messageText.contains(" ") ? messageText.split(" ")[0] : messageText;
        messageText = messageText.replace("/", "");
        commandHandler.get(messageText).handleUpdate(update, bot);
    }

    /**
     * Check if there's an hashtag in the current update and if it's inside {@link #hashHandler}
     * @param update The {@link Update} received from telegram
     * @return if the update contains an hashtag
     */
    private boolean isHash(Update update) {
        Message message = retrieveMessage(update);
        if (message == null) {
            return false;
        }
        String messageText = retrieveText(message);
        if (messageText == null) {
            return false;
        }
        final Pattern pattern = Pattern.compile(HASH_PATTERN, Pattern.DOTALL);
        final Matcher matcher = pattern.matcher(messageText);
        while (matcher.find()) {
            String hash = matcher.group(0).replaceFirst("#", "");
            if (hashHandler.containsKey(hash)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Search over {@link #hashHandler} for a {@link UpdateHandler} for the specific hashtag
     * @param update The {@link Update} received from telegram
     * @return void
     */
    private void handleHash(Update update) {
        Message message = retrieveMessage(update);
        final Pattern pattern = Pattern.compile(HASH_PATTERN, Pattern.DOTALL);
        final Matcher matcher = pattern.matcher(retrieveText(message));
        while (matcher.find()) {
            String hash = matcher.group(0).replaceFirst("#", "");
            if (hashHandler.containsKey(hash)) {
                hashHandler.get(hash).handleUpdate(update, bot);
                return;
            }
        }
    }

    /**
     * Check if the message contains a photo or a picture document
     * @param update The {@link Update} received from telegram
     * @return if the update has a photo and if there's an handler register for photos
     */
    private boolean isPhoto(Update update) {
        Message message = retrieveMessage(update);
        if (message == null) {
            return false;
        }
        Document document = message.document();
        if (document == null) {
            return false;
        }
        if (document.mimeType().contains("image")) {
            return true;
        }
        return message.photo() != null && this.photoHandler != null;
    }

    /**
     * Handle an update with a photo in it
     * @param update The {@link Update} received from telegram
     */
    private void handlePhoto(Update update) {
        this.photoHandler.handleUpdate(update, bot);
    }

    /**
     * Handle a generic update
     * @param update The {@link Update} received from telegram
     */
    private void handleDefault(Update update) {
        if (this.defaultHandler == null) {
            throw new RuntimeException("Unable to handle update, missing default handler for unknown update");
        }
        this.defaultHandler.handleUpdate(update, bot);
    }

    /**
     * Check if the message text matches any of the handler declared
     * @param update The {@link Update} received from telegram
     * @return if the message text matches any message handler
     */
    private boolean isMessage(Update update) {
        Message message = retrieveMessage(update);
        if (message == null) {
            return false;
        }
        String messageText = retrieveText(message);
        if (messageText == null) {
            return false;
        }
        return messageHandler.keySet().stream().anyMatch(regex -> {
            final Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
            return pattern.matcher(messageText).find();
        });
    }

    /**
     * Checks if there's a callback registered in the various handlers
     * @param update The {@link Update} received from telegram
     * @return
     */
    private boolean isCallback(Update update) {
        CallbackQuery callbackQuery = update.callbackQuery();
        if (callbackQuery == null) {
            return false;
        }
        String callback = callbackQuery.data();
        if (callback != null) {
            return callbackHandler.keySet().stream().anyMatch(regex -> {
                final Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
                return pattern.matcher(callback).find();
            });
        }
        return false;
    }

    /**
     * Handle a CallbackQuery
     * @param update The {@link Update} received from telegram
     */
    private void handleCallback(Update update) {
        String callback = update.callbackQuery().data();
        Optional<String> handlerKey = callbackHandler.keySet().stream().filter(regex -> {
            final Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
            return pattern.matcher(callback).find();
        }).findFirst();
        handlerKey.ifPresent(s -> callbackHandler.get(s).handleUpdate(update, bot));
    }

    /**
     * Handle a message update
     * @param update The {@link Update} received from telegram
     */
    private void handleMessage(Update update) {
        Message message = retrieveMessage(update);
        String messageText = retrieveText(message);
        Optional<String> handlerKey = messageHandler.keySet().stream().filter(regex -> {
            final Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
            return pattern.matcher(messageText).find();
        }).findFirst();
        handlerKey.ifPresent(s -> messageHandler.get(s).handleUpdate(update, bot));
    }

    /**
     * Checks if the user is sending updates about an inlineQuery
     * @param update The {@link Update} received from telegram
     * @return
     */
    private boolean isInlineQuery(Update update) {
        InlineQuery inlineQuery = update.inlineQuery();
        if (inlineQuery == null) {
            return false;
        }
        String queryString = inlineQuery.query();
        return queryString != null && inlineQueryHandler != null;
    }

    /**
     * Handle the inlineQuery update
     * @param update The {@link Update} received from telegram
     */
    private void handleInlineQuery(Update update) {
        inlineQueryHandler.handleUpdate(update, bot);
    }

    private void process(Update update) {
        if (isCommand(update)) {
            handleCommand(update);
        } else if (isHash(update)) {
            handleHash(update);
        } else if (isCallback(update)) {
            handleCallback(update);
        } else if (isInlineQuery(update)) {
            handleInlineQuery(update);
        } else if (isMessage(update)) {
            handleMessage(update);
        } else if (isPhoto(update)) {
            handlePhoto(update);
        } else {
            handleDefault(update);
        }
    }
}
