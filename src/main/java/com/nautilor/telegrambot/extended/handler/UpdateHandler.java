package com.nautilor.telegrambot.extended.handler;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Update;

public interface UpdateHandler {

    void handleUpdate(Update update, TelegramBot bot);
}
