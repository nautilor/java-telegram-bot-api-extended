# Java Telegram Bot Api Extended

This is an extension of [pengrad Telegram Bot Api](https://github.com/pengrad/java-telegram-bot-api)

This library will make your life easier by adding the possibility to

- Handle specific incoming commands
- Handle specific incoming messages
- Handle specific incoming hashtag
- Handle specific incoming photos (as a file or not)
- Handle specific incoming callbacks
- Handle incoming Inline Queries

# How To

## Declaring an UpdateHandler

UpdateHandler is the interface to implement to handle a single update

the declaration is really simple

```java
    public class MyUpdateHandler implements UpdateHandler {
        public void handleUpdate(Update update, TelegramBot bot) {
            // write you custom update implementation
        }
    }
```

You want to create an UpdateHandler for every single update you want to handle


## Run your Telegram Bot

To run your bot and receive updates just follow this simple example

```java
    // DefaultHandler.java - This is the mandatory class where all generic updates will be forwarded
    public class DefaultUpdateHandler implements UpdateHandler {
        public void handleUpdate(Update update, TelegramBot bot) {
            // write you custom update implementation
        }
    }

    // StartHandler.java - This is an example class where all the updates for the /start command will be forwarded
    public class StartHandler implements UpdateHandler {
        public void handleUpdate(Update update, TelegramBot bot) {
            // Example - The bot replies with a welcome message
            bot.execute(new SendMessage(update.message().chat().id(), "Hello and Welcome"));
        }
    }

    // The code below goes in the class where you want to start your bot - Ex. Main
    UpdateListener listner = new UpdateListener();

    // Set the TOKEN to use with the bot
    listener.setToken("TELEGRAM_TOKEN");

    // Add default handler for generic updates
    listner.addDefaultListener(new DefaultUpdateHandler());

    // Add handler for /start commands
    listener.addCommandHandler("start", new StartHandler());

    // Start the bot
    listner.listen();
```

## CommandHandler

CommandHandler is usefull to handle users commands in different classes without having to do
the parsing manually to check which command the user typed

### Syntax

```java
    listener.addCommandHandler("command", handler);
```

## MessageHandler

MessageHandler allow you to receive updates of specific messages that matches a regex

### Syntax

```java
    // match messages with letters only
    listener.addMessageHandler("[A-Za-z]", handler)

    // match messages with numbers only
    listener.addMessageHandler("[0-9]", handler)

    // match messages that contains the word Hello in the text
    listener.addMessageHandler("Hello", handler)

    // match messages that contains only the word Hello
    listener.addMessageHandler("^Hello$", handler)
```
> For the rest of the handler check the javadoc